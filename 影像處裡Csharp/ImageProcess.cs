﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace 影像處理Csharp
{
    public class ImageProcess
    {
        private Bitmap sourceImage;
        private Color sourColor;
        private int imgWidth, imgHeight;
        private byte[,] grayImage;

        // Image I/O
        internal void LoadImage(string filemane)
        {
            if (sourceImage != null) sourceImage.Dispose();
            sourceImage = new Bitmap(filemane);

            initializaGlobalVariable();
        }

        internal void LoadImage(Bitmap img)
        {
            if (sourceImage != null) sourceImage.Dispose();
            sourceImage = new Bitmap(img);

            initializaGlobalVariable();
        }

        internal Bitmap Image
        {
            get { return sourceImage; }
        }

        internal void windowShow(string windowName, Bitmap image)
        {
            Form imageForm = new Form();
            imageForm.BackgroundImageLayout = ImageLayout.Center;
            imageForm.ClientSize = new Size(imgWidth, imgHeight);
            imageForm.MaximizeBox = false;
            imageForm.FormBorderStyle = FormBorderStyle.FixedSingle;
            imageForm.Text = windowName;

            imageForm.BackgroundImage = image;
            imageForm.Show();
        }



        // Image process
        internal Bitmap GrayScale()
        {
            Bitmap grayImg = new Bitmap(imgWidth, imgHeight);
            Color grayColor;
            int graypix;

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    graypix = grayImage[x, y];
                    grayColor = Color.FromArgb(graypix, graypix, graypix);
                    grayImg.SetPixel(x, y, grayColor);
                }

            return grayImg;
        }

        internal Bitmap MeanFilter()
        {
            Bitmap meanImg = new Bitmap(imgWidth, imgHeight);
            int meanpix = 0;

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    meanpix = 0;
                    for (sbyte j = -1; j <= 1; j++)
                        for (sbyte i = -1; i <= 1; i++)
                            meanpix += grayImage[Nx(x + i), Ny(y + j)];
                    meanpix /= 9;

                    meanImg.SetPixel(x, y, Color.FromArgb(meanpix, meanpix, meanpix));
                }

            return meanImg;
        }

        internal Bitmap MedianFilter()
        {
            Bitmap medianImg = new Bitmap(imgWidth, imgHeight);
            byte[] filtwind = new byte[9];

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    for (sbyte j = -1, index = 0; j <= 1; j++)
                        for (sbyte i = -1; i <= 1; i++)
                            filtwind[index++] = grayImage[Nx(x + i), Ny(y + j)];
                    Array.Sort(filtwind);

                    medianImg.SetPixel(x, y, Color.FromArgb(filtwind[5], filtwind[5], filtwind[5]));
                }

            return medianImg;
        }

        internal Bitmap HistEqual()
        {
            Bitmap histImg = new Bitmap(imgWidth, imgHeight);
            int histpix, imgSize = imgWidth * imgHeight;
            int[] Weight = new int[256];

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    Weight[grayImage[x, y]] += 1;

            for (int i = 1; i < 256; i++)
                Weight[i] += Weight[i - 1];

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    histpix = (Weight[grayImage[x, y]] * 255) / imgSize;

                    histImg.SetPixel(x, y, Color.FromArgb(histpix, histpix, histpix));
                }

            return histImg;
        }

        internal Bitmap HSV(int H_offset, int S_offset, int V_offset)
        {
            Bitmap HSVImg = new Bitmap(imgWidth, imgHeight);
            Color hsvColor;
            double max, min, C;
            double H = 0.0, S, V;
            double R, G, B;
            double f;
            int hi, p, q, t;

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    sourColor = sourceImage.GetPixel(x, y);
                    R = sourColor.R / 255.0;
                    G = sourColor.G / 255.0;
                    B = sourColor.B / 255.0;

                    //encode
                    max = Math.Max(R, Math.Max(G, B));
                    min = Math.Min(R, Math.Min(G, B));
                    C = max - min;

                    if (max == min)
                        H = 0.0;
                    else if (max == R && G >= B)
                        H = 60.0 * (G - B) / C;
                    else if (max == R && G < B)
                        H = 60.0 * (G - B) / C + 360.0;
                    else if (max == G)
                        H = 60.0 * (B - R) / C + 120.0;
                    else if (max == B)
                        H = 60.0 * (R - G) / C + 240.0;

                    V = max;

                    if (max == 0.0)
                        S = 0.0;
                    else
                        S = C / V;

                    //offset
                    H = (H + H_offset) % 360;
                    S += S_offset / 100.0;
                    V += V_offset / 100.0;

                    if (S > 1) S = 1;
                    else if (S < 0) S = 0;

                    if (V > 1) V = 1;
                    else if (V < 0) V = 0;

                    //decode
                    hi = (int)(Math.Floor(H / 60.0)) % 6;
                    f = H / 60.0 - Math.Floor(H / 60.0);

                    V *= 255.0;
                    p = (int)(V * (1.0 - S));
                    q = (int)(V * (1.0 - f * S));
                    t = (int)(V * (1.0 - (1.0 - f) * S));

                    switch (hi)
                    {
                        case 0:
                            hsvColor = Color.FromArgb((int)V, t, p);
                            break;
                        case 1:
                            hsvColor = Color.FromArgb(q, (int)V, (int)p);
                            break;
                        case 2:
                            hsvColor = Color.FromArgb(p, (int)V, t);
                            break;
                        case 3:
                            hsvColor = Color.FromArgb(p, q, (int)V);
                            break;
                        case 4:
                            hsvColor = Color.FromArgb(t, p, (int)V);
                            break;
                        default:
                            hsvColor = Color.FromArgb((int)V, p, q);
                            break;
                    }

                    HSVImg.SetPixel(x, y, hsvColor);
                }

            return HSVImg;
        }

        internal Bitmap FuzzyMedianFilter()
        {
            Bitmap fuzymediaImg = new Bitmap(imgWidth, imgHeight);
            Color fuzymediaColor;
            int K = 1, MAXwindsid = 13, Weight = 168;

            int windsid = 2 * K + 1, n = windsid * windsid, index = 0;
            int Imax = 0, Imin = 0, Smax = 0, Smin = 0, Gradsum = 0, FuzzyNum = 0;
            int[] filterwind = new int[n];
            bool islambdaCbig = false;

            int i, j;
            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    windsid = 2 * K + 1; n = windsid * windsid; index = 0;
                    Imax = 0; Imin = 0; Smax = 0; Smin = 0; Gradsum = 0; FuzzyNum = 0;
                    filterwind = new int[n];
                    islambdaCbig = false;

                    for (j = -K; j <= K; j++)
                        for (i = -K; i <= K; i++)
                            if (x + i >= 0 && y + j >= 0 && x + i < sourceImage.Width && y + j < sourceImage.Height)
                            {
                                filterwind[index++] = grayImage[x + i, y + j];
                                Gradsum += S(x + i, y + j, K);
                            }

                    Array.Resize<int>(ref filterwind, index);
                    Imax = filterwind.Max();
                    Imin = filterwind.Min();

                    for (i = 0; i < index; i++)
                        if (filterwind[i] == Imax)
                            Smax++;
                        else if (filterwind[i] == Imin)
                            Smin++;

                    if ((Gradsum >= Weight * n) && (Smax > n / 2 || Smin > n / 2))
                        islambdaCbig = true;
                    else
                        islambdaCbig = false;

                    if (islambdaCbig && (Smax + Smin == n))
                        if (windsid < MAXwindsid)
                        {
                            K++;
                            x--;
                            continue;
                        }
                        else
                        {
                            if (Smin >= Smax)
                                FuzzyNum = Imin;
                            else
                                FuzzyNum = Imax;
                        }
                    else
                    {
                        Array.Sort(filterwind);
                        if ((Smax + Smin) > (n / 2))
                            FuzzyNum = filterwind[((index - Smax) / 2) + (Smin / 2)];
                        else
                            FuzzyNum = filterwind[index / 2];
                    }
                    K = 1;

                    fuzymediaColor = Color.FromArgb(FuzzyNum, FuzzyNum, FuzzyNum);
                    fuzymediaImg.SetPixel(x, y, fuzymediaColor);
                }

            return fuzymediaImg;
        }

        internal Bitmap HoughLines()
        {
            Bitmap lineImg = new Bitmap(imgWidth, imgHeight);
            //int rmax = (int)(Math.Sqrt(Math.Pow(imgWidth, 2) + Math.Pow(imgHeight, 2)));
            //int[,] accArry = new int[180, rmax];
            //int r;

            //for (int y = 0; y < imgHeight; y++)
            //    for (int x = 0; x < imgWidth; x++)
            //        if (grayImage[x, y] == 0)
            //        {
            //            for (int theta = 0; theta < 90; theta++)
            //            {
            //                r = (int)Math.Round(x * Math.Cos(theta * Math.PI / 180.0) + y * Math.Sin(theta * Math.PI / 180.0));
            //                accArry[theta, r]++;
            //            }
            //            for (int theta = 90; theta < 180; theta++)
            //            {
            //                r = (int)Math.Round(x * -Math.Cos(theta * Math.PI / 180.0) + y * Math.Sin(theta * Math.PI / 180.0));
            //                accArry[theta, r]++;
            //            }
            //        }

            drawLine(ref lineImg, 40, 40, 70, 11);

            //Bitmap tmp = new Bitmap(180, rmax);
            //Color lineColor;
            //for (int j = 0; j < rmax; j++)
            //    for (int i = 0; i < 180; i++)
            //    {
            //        lineColor = Color.FromArgb(accArry[i, j], accArry[i, j], accArry[i, j]);
            //        tmp.SetPixel(i, j, lineColor);
            //    }
            //windowShow("", tmp);
            //if (accArry[i, j] >= 1)
            //    drawLine(ref lineImg, j, i);

            return lineImg;
        }

        internal Bitmap MarrHildreth()
        {
            Bitmap harrImg = new Bitmap(imgWidth, imgHeight);
            Color harrColor;
            int sum = 0, index = 0;
            sbyte[] mask = new sbyte[25] {  0,  0, -1,  0,  0,
                                            0, -1, -2, -1,  0,
                                           -1, -2, 16, -2, -1,
                                            0, -1, -2, -1,  0,
                                            0,  0, -1,  0,  0 };

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    sum = index = 0;
                    for (int j = -2; j <= 2; j++)
                        for (int i = -2; i <= 2; i++)
                            sum += grayImage[Nx(x + i), Ny(y + j)] * mask[index++];
                    sum = (sum < 0 ? 0 : (sum > 255 ? 255 : sum));

                    harrColor = Color.FromArgb(sum, sum, sum);
                    harrImg.SetPixel(x, y, harrColor);
                }

            return harrImg;
        }

        internal Bitmap Otsu()
        {
            int imgSize = imgWidth * imgHeight, W1 = 0, W2 = 0;
            int[] weight = new int[256];
            byte threshold = 0;
            double Mean = 0.0;
            double u1 = 0.0, u2 = 0.0, sigma = 0.0, Maxsigma = 0.0;

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    Mean += grayImage[x, y];
                    weight[grayImage[x, y]]++;
                }
            Mean /= imgSize;

            for (int T = 0; T <= 255; T++)
            {
                if (weight[T] == 0) continue;

                W1 = W2 = 0;
                for (int i = 0; i <= T; i++)
                    W1 += weight[i];
                for (int i = T + 1; i <= 255; i++)
                    W2 += weight[i];

                u1 = u2 = 0;
                for (int i = 0; i <= T; i++)
                    u1 += weight[i] / (double)W1 * i;
                for (int i = T + 1; i <= 255; i++)
                    u2 += weight[i] / (double)W2 * i;

                sigma = W1 * Math.Pow(u1 - Mean, 2) + W2 * Math.Pow(u2 - Mean, 2);
                if (sigma > Maxsigma)
                {
                    Maxsigma = sigma;
                    threshold = (byte)T;
                }
            }

            return Thresholding(threshold);
        }

        internal Bitmap Kapur()
        {
            int W1 = 0, W2 = 0;
            int[] P = new int[256];
            byte threshold = 0;
            double E1 = 0.0, E2 = 0.0, sigma = 0.0, Maxsigma = 0.0;

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    P[grayImage[x, y]]++;

            for (int T = 0; T <= 255; T++)
            {
                if (P[T] == 0) continue;

                W1 = W2 = 0;
                for (int i = 0; i <= T; i++)
                    W1 += P[i];
                for (int i = T + 1; i <= 255; i++)
                    W2 += P[i];

                E1 = E2 = 0.0;
                for (int i = 0; i <= T; i++)
                    if (P[i] != 0)
                        E1 += (P[i] / (double)W1) * Math.Log((W1 / (double)P[i]), 2);
                for (int i = T + 1; i <= 255; i++)
                    if (P[i] != 0)
                        E2 += (P[i] / (double)W2) * Math.Log((W2 / (double)P[i]), 2);

                sigma = E1 + E2;
                if (sigma > Maxsigma)
                {
                    Maxsigma = sigma;
                    threshold = (byte)T;
                }
            }

            return Thresholding(threshold);
        }

        internal Bitmap Thresholding(byte threshold)
        {
            Bitmap threshImg = new Bitmap(imgWidth, imgHeight);
            Color threshColor;

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    if (grayImage[x, y] > threshold)
                        threshColor = Color.FromArgb(255, 255, 255);
                    else
                        threshColor = Color.FromArgb(0, 0, 0);

                    threshImg.SetPixel(x, y, threshColor);
                }

            return threshImg;
        }

        internal Bitmap Connected()
        {
            Bitmap connectImg = Otsu();
            short[,] map = new short[imgWidth, imgHeight];

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    if (connectImg.GetPixel(x, y).R == 255)
                        map[x, y] = -2;
                    else
                        map[x, y] = -1;

            short blockIndex = 0;
            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    if (map[x, y] == -1)
                    {
                        map[x, y] = blockIndex++;
                        for (sbyte j = -1; j <= 0; j++)
                            for (sbyte i = -1; i <= 1; i++)
                                if (map[Nx(x + i), Ny(y + j)] >= 0 && map[Nx(x + i), Ny(y + j)] < map[x, y])
                                    map[x, y] = map[Nx(x + i), Ny(y + j)];
                        if (map[x, y] != blockIndex - 1) blockIndex--;
                    }

            for (int y = imgHeight - 1; y >= 0; y--)
                for (int x = imgWidth - 1; x >= 0; x--)
                    if (map[x, y] >= 0)
                    {
                        for (sbyte j = 0; j <= 1; j++)
                            for (sbyte i = -1; i <= 1; i++)
                                if (map[Nx(x + i), Ny(y + j)] >= 0 && map[Nx(x + i), Ny(y + j)] < map[x, y])
                                    map[x, y] = map[Nx(x + i), Ny(y + j)];
                    }

            Random blockRan = new Random();
            byte[,] blockColor = new byte[blockIndex, 3];
            for (short i = 0; i < blockIndex; i++)
                for (sbyte j = 0; j < 3; j++)
                    blockColor[i, j] = (byte)blockRan.Next(0, 255);

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    if (map[x, y] >= 0)
                    {
                        connectImg.SetPixel(x, y, Color.FromArgb(blockColor[map[x, y], 0], blockColor[map[x, y], 1], blockColor[map[x, y], 2]));
                    }

            return connectImg;
        }

        internal Bitmap Connrcted_Recursive()
        {
            Bitmap connectImg = Otsu();
            short[,] map = new short[imgWidth, imgHeight];

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    if (connectImg.GetPixel(x, y).R == 255)
                        map[x, y] = -2;
                    else
                        map[x, y] = -1;

            short blockIndex = 0;
            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    if (map[x, y] == -1)
                    {
                        ConRecur(ref map, x, y, blockIndex);
                        blockIndex++;
                    }

            Random blockRan = new Random();
            byte[,] blockColor = new byte[blockIndex, 3];
            for (short i = 0; i < blockIndex; i++)
                for (sbyte j = 0; j < 3; j++)
                    blockColor[i, j] = (byte)blockRan.Next(0, 255);

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    if (map[x, y] >= 0)
                    {
                        connectImg.SetPixel(x, y, Color.FromArgb(blockColor[map[x, y], 0], blockColor[map[x, y], 1], blockColor[map[x, y], 2]));
                    }

            return connectImg;
        }

        internal Bitmap Watershed()
        {
            Bitmap watershedImg = GrayScale();
            byte[,] map = new byte[imgWidth, imgHeight];

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    byte tmp = grayImage[x, y], dir = 0;
                    for (sbyte j = -1; j <= 1; j++)
                        for (sbyte i = -1; i <= 1; i++, dir++)
                            if (tmp > grayImage[Nx(x + i), Ny(y + j)])
                            {
                                map[x, y] = dir;
                                tmp = grayImage[Nx(x + i), Ny(y + j)];
                            }
                    if (grayImage[x, y] == tmp) map[x, y] = 4;

                    //MessageBox.Show(map[x, y] + " " + grayImage[x, y]);
                }

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    switch (map[x, y])
                    {
                        case 0:
                            if (map[Nx(x + 1), Ny(y)] == 6 ||
                                map[Nx(x + 1), Ny(y)] == 7 ||
                                map[Nx(x + 1), Ny(y)] == 8 ||

                                map[Nx(x + 1), Ny(y + 1)] == 6 ||
                                map[Nx(x + 1), Ny(y + 1)] == 7 ||
                                map[Nx(x + 1), Ny(y + 1)] == 8 ||

                                map[Nx(x), Ny(y + 1)] == 6 ||
                                map[Nx(x), Ny(y + 1)] == 7 ||
                                map[Nx(x), Ny(y + 1)] == 8)

                                watershedImg.SetPixel(x, y, Color.FromArgb(255, 0, 0));
                            break;
                        case 1:
                            if (map[Nx(x - 1), Ny(y + 1)] == 6 ||
                                map[Nx(x - 1), Ny(y + 1)] == 7 ||
                                map[Nx(x - 1), Ny(y + 1)] == 8 ||

                                map[Nx(x), Ny(y + 1)] == 6 ||
                                map[Nx(x), Ny(y + 1)] == 7 ||
                                map[Nx(x), Ny(y + 1)] == 8 ||

                                map[Nx(x + 1), Ny(y + 1)] == 6 ||
                                map[Nx(x + 1), Ny(y + 1)] == 7 ||
                                map[Nx(x + 1), Ny(y + 1)] == 8)

                                watershedImg.SetPixel(x, y, Color.FromArgb(255, 0, 0));
                            break;
                        case 2:
                            if (map[Nx(x - 1), Ny(y)] == 6 ||
                                map[Nx(x - 1), Ny(y)] == 7 ||
                                map[Nx(x - 1), Ny(y)] == 8 ||

                                map[Nx(x - 1), Ny(y + 1)] == 6 ||
                                map[Nx(x - 1), Ny(y + 1)] == 7 ||
                                map[Nx(x - 1), Ny(y + 1)] == 8 ||

                                map[Nx(x), Ny(y + 1)] == 6 ||
                                map[Nx(x), Ny(y + 1)] == 7 ||
                                map[Nx(x), Ny(y + 1)] == 8)

                                watershedImg.SetPixel(x, y, Color.FromArgb(255, 0, 0));
                            break;
                        case 3:
                            if (map[Nx(x + 1), Ny(y - 1)] == 6 ||
                                map[Nx(x + 1), Ny(y - 1)] == 7 ||
                                map[Nx(x + 1), Ny(y - 1)] == 8 ||

                                map[Nx(x + 1), Ny(y)] == 6 ||
                                map[Nx(x + 1), Ny(y)] == 7 ||
                                map[Nx(x + 1), Ny(y)] == 8 ||

                                map[Nx(x + 1), Ny(y + 1)] == 6 ||
                                map[Nx(x + 1), Ny(y + 1)] == 7 ||
                                map[Nx(x + 1), Ny(y + 1)] == 8)

                                watershedImg.SetPixel(x, y, Color.FromArgb(255, 0, 0));
                            break;
                        case 5:
                            if (map[Nx(x - 1), Ny(y - 1)] == 6 ||
                                map[Nx(x - 1), Ny(y - 1)] == 7 ||
                                map[Nx(x - 1), Ny(y - 1)] == 8 ||

                                map[Nx(x - 1), Ny(y)] == 6 ||
                                map[Nx(x - 1), Ny(y)] == 7 ||
                                map[Nx(x - 1), Ny(y)] == 8 ||

                                map[Nx(x - 1), Ny(y + 1)] == 6 ||
                                map[Nx(x - 1), Ny(y + 1)] == 7 ||
                                map[Nx(x - 1), Ny(y + 1)] == 8)

                                watershedImg.SetPixel(x, y, Color.FromArgb(255, 0, 0));
                            break;
                        case 6:
                            if (map[Nx(x), Ny(y - 1)] == 6 ||
                                map[Nx(x), Ny(y - 1)] == 7 ||
                                map[Nx(x), Ny(y - 1)] == 8 ||

                                map[Nx(x + 1), Ny(y - 1)] == 6 ||
                                map[Nx(x + 1), Ny(y - 1)] == 7 ||
                                map[Nx(x + 1), Ny(y - 1)] == 8 ||

                                map[Nx(x + 1), Ny(y)] == 6 ||
                                map[Nx(x + 1), Ny(y)] == 7 ||
                                map[Nx(x + 1), Ny(y)] == 8)

                                watershedImg.SetPixel(x, y, Color.FromArgb(255, 0, 0));
                            break;
                        case 7:
                            if (map[Nx(x - 1), Ny(y - 1)] == 6 ||
                                map[Nx(x - 1), Ny(y - 1)] == 7 ||
                                map[Nx(x - 1), Ny(y - 1)] == 8 ||

                                map[Nx(x), Ny(y - 1)] == 6 ||
                                map[Nx(x), Ny(y - 1)] == 7 ||
                                map[Nx(x), Ny(y - 1)] == 8 ||

                                map[Nx(x + 1), Ny(y - 1)] == 6 ||
                                map[Nx(x + 1), Ny(y - 1)] == 7 ||
                                map[Nx(x + 1), Ny(y - 1)] == 8)

                                watershedImg.SetPixel(x, y, Color.FromArgb(255, 0, 0));
                            break;
                        case 8:
                            if (map[Nx(x - 1), Ny(y - 1)] == 6 ||
                                map[Nx(x - 1), Ny(y - 1)] == 7 ||
                                map[Nx(x - 1), Ny(y - 1)] == 8 ||

                                map[Nx(x), Ny(y - 1)] == 6 ||
                                map[Nx(x), Ny(y - 1)] == 7 ||
                                map[Nx(x), Ny(y - 1)] == 8 ||

                                map[Nx(x - 1), Ny(y)] == 6 ||
                                map[Nx(x - 1), Ny(y)] == 7 ||
                                map[Nx(x - 1), Ny(y)] == 8)

                                watershedImg.SetPixel(x, y, Color.FromArgb(255, 0, 0));
                            break;
                    }
                }

            return watershedImg;
        }

        internal Bitmap KmeanClustering(byte K)
        {
            Bitmap KmeanImg = Otsu();

            Point[] clusterCenter = new Point[K];
            Random random = new Random();
            for (byte i = 0; i < K; i++)
            {
                clusterCenter[i].X = random.Next(0, imgWidth - 1);
                clusterCenter[i].Y = random.Next(0, imgHeight - 1);
            }

            byte[,] map = new byte[imgWidth, imgHeight];
            bool isConvergence = false;
            while (!isConvergence)
            {
                Point[] point = new Point[K];
                int[] amount = new int[K];
                for (byte i = 0; i < K; i++) amount[i] = 1;

                for (int y = 0; y < imgHeight; y++)
                    for (int x = 0; x < imgWidth; x++)
                        if (KmeanImg.GetPixel(x, y).R == 0)
                        {
                            double nearest = -1.0;
                            byte cluster = 1;
                            for (byte i = 0, j = 1; i < K; i++, j++)
                            {
                                double near = Math.Pow(clusterCenter[i].X - x, 2) + Math.Pow(clusterCenter[i].Y - y, 2);
                                if (near < nearest || nearest == -1.0)
                                {
                                    cluster = j;
                                    nearest = near;
                                }
                            }
                            map[x, y] = cluster;
                            point[cluster - 1].X += x;
                            point[cluster - 1].Y += y;
                            amount[cluster - 1]++;
                        }

                isConvergence = true;
                for (byte i = 0; i < K; i++)
                    if (clusterCenter[i].X != point[i].X / amount[i] || clusterCenter[i].Y != point[i].Y / amount[i])
                    {
                        clusterCenter[i].X = point[i].X / amount[i];
                        clusterCenter[i].Y = point[i].Y / amount[i];

                        isConvergence = false;
                    }
            }

            Color[] color = new Color[256];
            for (ushort i = 0; i < 256; i++)
                color[i] = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));

            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    if (map[x, y] != 0)
                        KmeanImg.SetPixel(x, y, color[map[x, y]]);

            return KmeanImg;
        }

        internal Bitmap CmeanClustering(byte C)
        {
            Bitmap CmeanImg = Otsu();
            bool isConvergence = false;
            sbyte m = 2;

            UInt32 NumberOfBlackPix = 0;
            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    if (CmeanImg.GetPixel(x, y).R == 0)
                        NumberOfBlackPix++;

            Point[] BlackPoint = new Point[NumberOfBlackPix];
            for (int y = 0, index = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    if (CmeanImg.GetPixel(x, y).R == 0)
                    {
                        BlackPoint[index].X = x;
                        BlackPoint[index++].Y = y;
                    }

            Point[] clusterCenter = new Point[C];
            Random random = new Random();
            for (byte index = 0; index < C; index++)
                clusterCenter[index] = BlackPoint[random.Next(0, BlackPoint.Length - 1)];

            double[,] U = new double[C, NumberOfBlackPix];
            //////////////////////////////////////////////////////
            while (!isConvergence)
            {
                for (int j = 0; j < NumberOfBlackPix; j++)
                    for (int i = 0; i < C; i++)
                    {
                        double temp = 0.0;
                        for (int k = 0; k < C; k++)
                            temp += Math.Pow(getDistanceOfTwoPoint(BlackPoint[j], clusterCenter[i]) /
                                getDistanceOfTwoPoint(BlackPoint[j], clusterCenter[k]), 2.0 / (m - 1.0));
                        if (temp > 0)
                            U[i, j] = 1.0 / temp;
                        else
                            U[i, j] = 1;
                    }

                double[] tmp = new double[3];
                Point[] afterCenter = new Point[C];
                for (int i = 0; i < C; i++)
                {
                    for (int j = 0; j < NumberOfBlackPix; j++)
                    {
                        tmp[0] += Math.Pow(U[i, j], m) * BlackPoint[i].X;
                        tmp[1] += Math.Pow(U[i, j], m) * BlackPoint[i].Y;
                        tmp[2] += Math.Pow(U[i, j], m);
                    }

                    afterCenter[i].X = (int)Math.Floor(tmp[0] / tmp[2]);
                    afterCenter[i].Y = (int)Math.Floor(tmp[1] / tmp[2]);
                }

                  isConvergence = true;
                for (byte i = 0; i < C; i++)
                    if (getDistanceOfTwoPoint(clusterCenter[i], afterCenter[i]) != 0)
                    {
                        isConvergence = false;
                        clusterCenter[i].X = afterCenter[i].X;
                        clusterCenter[i].Y = afterCenter[i].Y;
                    }
            }

            Color[] color = new Color[256];
            for (ushort i = 0; i < 256; i++)
                color[i] = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));

            for (int i = 0; i < C; i++)
                CmeanImg.SetPixel(clusterCenter[i].X, clusterCenter[i].Y, Color.FromArgb(255, 0, 0));

            return CmeanImg;
        }


        // internal function
        private void initializaGlobalVariable()
        {
            imgWidth = sourceImage.Width;
            imgHeight = sourceImage.Height;

            grayImage = new byte[imgWidth, imgHeight];
            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                {
                    sourColor = sourceImage.GetPixel(x, y);
                    grayImage[x, y] = (byte)(sourColor.R * 0.299 + sourColor.G * 0.587 + sourColor.B * 0.114);
                }
        }

        private int Nx(int x)
        {
            return x >= 0 ? (x < imgWidth ? x : imgWidth - 1) : 0;
        }

        private int Ny(int y)
        {
            return y >= 0 ? (y < imgHeight ? y : imgHeight - 1) : 0;
        }

        private int S(int x, int y, int k)
        {
            int Sum = 0;

            Sum += DeltaDI(x - k, y + k, k);
            Sum += DeltaDI(x + k, y + k, k);
            Sum += DeltaDI(x, y + k, k);
            Sum += DeltaDI(x + k, y, k);

            return Sum;
        }

        private int DeltaDI(int x, int y, int k)
        {
            int Sum = 0, Center = 0, Side = 0;

            if ((x < 0) || (y < 0) || (x >= imgWidth) || (y >= imgHeight))
                return 0;

            Center = grayImage[x, y];
            for (int j = -k; j <= k; j++)
                for (int i = -k; i <= k; i++)
                    if ((x + i >= 0) && (y + j >= 0) && (x + i < imgWidth) && (y + j < imgHeight))
                    {
                        Side = grayImage[x + i, y + j];
                        Sum += Math.Abs(Side - Center);
                    }

            return Sum;
        }

        private void drawLine(ref Bitmap img, int x1, int y1, int x2, int y2)
        {
            for (int y = 0; y < imgHeight; y++)
                for (int x = 0; x < imgWidth; x++)
                    if ((x2 - x1) * y + (y1 - y2) * x + x1 * y2 - x2 * y1 == 0)
                        img.SetPixel(x, y, Color.FromArgb(0, 0, 0));
        }

        private void ConRecur(ref short[,] map, int x, int y, short blockIndex)
        {
            int[,] point = new int[imgWidth * imgHeight, 2];
            uint index = 0;

            map[x, y] = blockIndex;
            point[index, 0] = x; point[index, 1] = y;

            for (sbyte j = -1; j <= 1; j++)
                for (sbyte i = -1; i <= 1; i++)
                {
                    if (map[Nx(x + i), Ny(y + j)] == -1)
                    {
                        index++;

                        map[Nx(x + i), Ny(y + j)] = blockIndex;
                        point[index, 0] = Nx(x + i); point[index, 1] = Ny(y + j);

                        x = point[index, 0]; y = point[index, 1];
                        j = -2;
                        //因為for每次都會遞增1，所以要設成-2，這樣+1後才會變成-1。
                        break;
                    }

                    if (j == 1 && i == 1 && index > 0)
                    {
                        j = -2;

                        index--;
                        x = point[index, 0]; y = point[index, 1];
                        break;
                    }
                }
        }

        private double getDistanceOfTwoPoint(Point point1, Point point2)
        {
            return Math.Sqrt(Math.Pow(point2.X - point1.X, 2) + Math.Pow(point2.Y - point1.Y, 2));
        }
        ///////////////////////
    }
}
