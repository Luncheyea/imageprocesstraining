﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 影像處理Csharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        ImageProcess imageprocess = new ImageProcess();

        private void Form1_Load(object sender, EventArgs e)
        {
            imageprocess.LoadImage(Resource1.NoImage);
        }

        private void LoadImageButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ImageFilePath = new OpenFileDialog();

            ImageFilePath.Filter = "Image Files|*.png; *.jpg; *.bmp|All files|*.*";
            if (ImageFilePath.ShowDialog() == DialogResult.OK)
            {
                imageprocess.LoadImage(ImageFilePath.FileName);
                imageprocess.windowShow("Original Image", imageprocess.Image);
            }

            ImageFilePath.Dispose();
        }

        private void ShowOriginalImageButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Original Image", imageprocess.Image);
        }

        private void GrayScaleButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Gray Scale Image", imageprocess.GrayScale());
            GC.Collect();
        }

        private void MeanFilterButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Mean Filter Image", imageprocess.MeanFilter());
            GC.Collect();
        }

        private void MedianFilterButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Median Filter Image", imageprocess.MedianFilter());
            GC.Collect();
        }

        private void FuzzyMedianFilterButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Fuzzy Median Filter Image", imageprocess.FuzzyMedianFilter());
            GC.Collect();
        }

        private void HistogramEqualizationButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Histogram Equalization Image", imageprocess.HistEqual());
            GC.Collect();
        }

        private void HSVButton_Click(object sender, EventArgs e)
        {
            int H = (int)numericUpDownH.Value;
            int S = (int)numericUpDownS.Value;
            int V = (int)numericUpDownV.Value;

            imageprocess.windowShow(String.Format("HSV Image  H:{0}, S:{1}, V:{2}", H, S, V), imageprocess.HSV(H, S, V));
            GC.Collect();
        }

        private void HoughLinesButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Hough Lines Image", imageprocess.HoughLines());
            GC.Collect();
        }

        private void MarrHildrethButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Marr Hildreth Image", imageprocess.MarrHildreth());
            GC.Collect();
        }

        private void ThresholdingButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow(String.Format("Thresholding Image  Threshold:{0}", numericUpDownThreshold.Value), imageprocess.Thresholding((byte)numericUpDownThreshold.Value));
            GC.Collect();
        }

        private void OtsuButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Otsu Thresholding Image", imageprocess.Otsu());
            GC.Collect();
        }

        private void KapurButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Kapur Thresholding Image", imageprocess.Kapur());
            GC.Collect();
        }

        private void ConnectedButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Connected-component labeling Image", imageprocess.Connected());
            GC.Collect();
        }

        private void ConnectedRecursiveButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Connected-component labeling (using Recursive) Image", imageprocess.Connrcted_Recursive());
            GC.Collect();
        }

        private void WatershedButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow("Watershed Image", imageprocess.Watershed());
            GC.Collect();
        }

        private void KmeanButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow(String.Format("K-mean clustering Image  K={0}", numericUpDownK.Value), imageprocess.KmeanClustering((byte)numericUpDownK.Value));
            GC.Collect();
        }

        private void FuzzyCmeanButton_Click(object sender, EventArgs e)
        {
            imageprocess.windowShow(String.Format("C-mean clustering Image  K={0}", numericUpDownK.Value), imageprocess.CmeanClustering((byte)numericUpDownK.Value));
            GC.Collect();
        }

        /////////////////////////
    }
}
