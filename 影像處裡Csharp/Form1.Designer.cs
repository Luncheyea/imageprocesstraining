﻿using System.Windows.Forms;

namespace 影像處理Csharp
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.LoadImageButton = new System.Windows.Forms.Button();
            this.GrayScaleButton = new System.Windows.Forms.Button();
            this.MeanFilterButton = new System.Windows.Forms.Button();
            this.HistogramEqualizationButton = new System.Windows.Forms.Button();
            this.HSVButton = new System.Windows.Forms.Button();
            this.FuzzyMedianFilterButton = new System.Windows.Forms.Button();
            this.numericUpDownH = new System.Windows.Forms.NumericUpDown();
            this.ShowOriginalImageButton = new System.Windows.Forms.Button();
            this.labelH = new System.Windows.Forms.Label();
            this.labelS = new System.Windows.Forms.Label();
            this.numericUpDownS = new System.Windows.Forms.NumericUpDown();
            this.labelV = new System.Windows.Forms.Label();
            this.numericUpDownV = new System.Windows.Forms.NumericUpDown();
            this.MedianFilterButton = new System.Windows.Forms.Button();
            this.HoughLinesButton = new System.Windows.Forms.Button();
            this.MarrHildrethButton = new System.Windows.Forms.Button();
            this.ThresholdingButton = new System.Windows.Forms.Button();
            this.numericUpDownThreshold = new System.Windows.Forms.NumericUpDown();
            this.labelThreshold = new System.Windows.Forms.Label();
            this.OtsuButton = new System.Windows.Forms.Button();
            this.KapurButton = new System.Windows.Forms.Button();
            this.ConnectedButton = new System.Windows.Forms.Button();
            this.ConnectedRecursiveButton = new System.Windows.Forms.Button();
            this.WatershedButton = new System.Windows.Forms.Button();
            this.KmeanButton = new System.Windows.Forms.Button();
            this.KDtreeButton = new System.Windows.Forms.Button();
            this.FuzzyCmeanButton = new System.Windows.Forms.Button();
            this.numericUpDownK = new System.Windows.Forms.NumericUpDown();
            this.labelK = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownK)).BeginInit();
            this.SuspendLayout();
            // 
            // LoadImageButton
            // 
            this.LoadImageButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.LoadImageButton.Location = new System.Drawing.Point(12, 33);
            this.LoadImageButton.Name = "LoadImageButton";
            this.LoadImageButton.Size = new System.Drawing.Size(130, 48);
            this.LoadImageButton.TabIndex = 0;
            this.LoadImageButton.Text = "載入圖片";
            this.LoadImageButton.UseVisualStyleBackColor = true;
            this.LoadImageButton.Click += new System.EventHandler(this.LoadImageButton_Click);
            // 
            // GrayScaleButton
            // 
            this.GrayScaleButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.GrayScaleButton.Location = new System.Drawing.Point(12, 122);
            this.GrayScaleButton.Name = "GrayScaleButton";
            this.GrayScaleButton.Size = new System.Drawing.Size(130, 48);
            this.GrayScaleButton.TabIndex = 2;
            this.GrayScaleButton.Text = "灰階";
            this.GrayScaleButton.UseVisualStyleBackColor = true;
            this.GrayScaleButton.Click += new System.EventHandler(this.GrayScaleButton_Click);
            // 
            // MeanFilterButton
            // 
            this.MeanFilterButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MeanFilterButton.Location = new System.Drawing.Point(12, 189);
            this.MeanFilterButton.Name = "MeanFilterButton";
            this.MeanFilterButton.Size = new System.Drawing.Size(130, 48);
            this.MeanFilterButton.TabIndex = 4;
            this.MeanFilterButton.Text = "均值濾波";
            this.MeanFilterButton.UseVisualStyleBackColor = true;
            this.MeanFilterButton.Click += new System.EventHandler(this.MeanFilterButton_Click);
            // 
            // HistogramEqualizationButton
            // 
            this.HistogramEqualizationButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.HistogramEqualizationButton.Location = new System.Drawing.Point(12, 256);
            this.HistogramEqualizationButton.Name = "HistogramEqualizationButton";
            this.HistogramEqualizationButton.Size = new System.Drawing.Size(148, 48);
            this.HistogramEqualizationButton.TabIndex = 7;
            this.HistogramEqualizationButton.Text = "直方圖等化";
            this.HistogramEqualizationButton.UseVisualStyleBackColor = true;
            this.HistogramEqualizationButton.Click += new System.EventHandler(this.HistogramEqualizationButton_Click);
            // 
            // HSVButton
            // 
            this.HSVButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.HSVButton.Location = new System.Drawing.Point(12, 322);
            this.HSVButton.Name = "HSVButton";
            this.HSVButton.Size = new System.Drawing.Size(130, 48);
            this.HSVButton.TabIndex = 8;
            this.HSVButton.Text = "HSV";
            this.HSVButton.UseVisualStyleBackColor = true;
            this.HSVButton.Click += new System.EventHandler(this.HSVButton_Click);
            // 
            // FuzzyMedianFilterButton
            // 
            this.FuzzyMedianFilterButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FuzzyMedianFilterButton.Location = new System.Drawing.Point(284, 189);
            this.FuzzyMedianFilterButton.Name = "FuzzyMedianFilterButton";
            this.FuzzyMedianFilterButton.Size = new System.Drawing.Size(177, 48);
            this.FuzzyMedianFilterButton.TabIndex = 6;
            this.FuzzyMedianFilterButton.Text = "模糊中值濾波";
            this.FuzzyMedianFilterButton.UseVisualStyleBackColor = true;
            this.FuzzyMedianFilterButton.Click += new System.EventHandler(this.FuzzyMedianFilterButton_Click);
            // 
            // numericUpDownH
            // 
            this.numericUpDownH.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.numericUpDownH.Location = new System.Drawing.Point(197, 322);
            this.numericUpDownH.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDownH.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.numericUpDownH.Name = "numericUpDownH";
            this.numericUpDownH.Size = new System.Drawing.Size(85, 39);
            this.numericUpDownH.TabIndex = 9;
            this.numericUpDownH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ShowOriginalImageButton
            // 
            this.ShowOriginalImageButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ShowOriginalImageButton.Location = new System.Drawing.Point(148, 33);
            this.ShowOriginalImageButton.Name = "ShowOriginalImageButton";
            this.ShowOriginalImageButton.Size = new System.Drawing.Size(130, 48);
            this.ShowOriginalImageButton.TabIndex = 1;
            this.ShowOriginalImageButton.Text = "顯示原圖";
            this.ShowOriginalImageButton.UseVisualStyleBackColor = true;
            this.ShowOriginalImageButton.Click += new System.EventHandler(this.ShowOriginalImageButton_Click);
            // 
            // labelH
            // 
            this.labelH.AutoSize = true;
            this.labelH.Font = new System.Drawing.Font("Microsoft JhengHei", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.labelH.Location = new System.Drawing.Point(148, 333);
            this.labelH.Name = "labelH";
            this.labelH.Size = new System.Drawing.Size(43, 25);
            this.labelH.TabIndex = 30;
            this.labelH.Text = "H : ";
            // 
            // labelS
            // 
            this.labelS.AutoSize = true;
            this.labelS.Font = new System.Drawing.Font("Microsoft JhengHei", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.labelS.Location = new System.Drawing.Point(288, 333);
            this.labelS.Name = "labelS";
            this.labelS.Size = new System.Drawing.Size(39, 25);
            this.labelS.TabIndex = 31;
            this.labelS.Text = "S : ";
            // 
            // numericUpDownS
            // 
            this.numericUpDownS.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.numericUpDownS.Location = new System.Drawing.Point(329, 322);
            this.numericUpDownS.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownS.Name = "numericUpDownS";
            this.numericUpDownS.Size = new System.Drawing.Size(85, 39);
            this.numericUpDownS.TabIndex = 10;
            this.numericUpDownS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelV
            // 
            this.labelV.AutoSize = true;
            this.labelV.Font = new System.Drawing.Font("Microsoft JhengHei", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.labelV.Location = new System.Drawing.Point(420, 333);
            this.labelV.Name = "labelV";
            this.labelV.Size = new System.Drawing.Size(41, 25);
            this.labelV.TabIndex = 32;
            this.labelV.Text = "V : ";
            // 
            // numericUpDownV
            // 
            this.numericUpDownV.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.numericUpDownV.Location = new System.Drawing.Point(467, 322);
            this.numericUpDownV.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.numericUpDownV.Name = "numericUpDownV";
            this.numericUpDownV.Size = new System.Drawing.Size(85, 39);
            this.numericUpDownV.TabIndex = 11;
            this.numericUpDownV.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // MedianFilterButton
            // 
            this.MedianFilterButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MedianFilterButton.Location = new System.Drawing.Point(148, 189);
            this.MedianFilterButton.Name = "MedianFilterButton";
            this.MedianFilterButton.Size = new System.Drawing.Size(130, 48);
            this.MedianFilterButton.TabIndex = 5;
            this.MedianFilterButton.Text = "中值濾波";
            this.MedianFilterButton.UseVisualStyleBackColor = true;
            this.MedianFilterButton.Click += new System.EventHandler(this.MedianFilterButton_Click);
            // 
            // HoughLinesButton
            // 
            this.HoughLinesButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.HoughLinesButton.Location = new System.Drawing.Point(12, 389);
            this.HoughLinesButton.Name = "HoughLinesButton";
            this.HoughLinesButton.Size = new System.Drawing.Size(130, 48);
            this.HoughLinesButton.TabIndex = 12;
            this.HoughLinesButton.Text = "霍夫找線";
            this.HoughLinesButton.UseVisualStyleBackColor = true;
            this.HoughLinesButton.Click += new System.EventHandler(this.HoughLinesButton_Click);
            // 
            // MarrHildrethButton
            // 
            this.MarrHildrethButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.MarrHildrethButton.Location = new System.Drawing.Point(12, 456);
            this.MarrHildrethButton.Name = "MarrHildrethButton";
            this.MarrHildrethButton.Size = new System.Drawing.Size(130, 48);
            this.MarrHildrethButton.TabIndex = 13;
            this.MarrHildrethButton.Text = "Harr測邊";
            this.MarrHildrethButton.UseVisualStyleBackColor = true;
            this.MarrHildrethButton.Click += new System.EventHandler(this.MarrHildrethButton_Click);
            // 
            // ThresholdingButton
            // 
            this.ThresholdingButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ThresholdingButton.Location = new System.Drawing.Point(284, 522);
            this.ThresholdingButton.Name = "ThresholdingButton";
            this.ThresholdingButton.Size = new System.Drawing.Size(130, 48);
            this.ThresholdingButton.TabIndex = 16;
            this.ThresholdingButton.Text = "二值化";
            this.ThresholdingButton.UseVisualStyleBackColor = true;
            this.ThresholdingButton.Click += new System.EventHandler(this.ThresholdingButton_Click);
            // 
            // numericUpDownThreshold
            // 
            this.numericUpDownThreshold.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.numericUpDownThreshold.Location = new System.Drawing.Point(486, 521);
            this.numericUpDownThreshold.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownThreshold.Name = "numericUpDownThreshold";
            this.numericUpDownThreshold.Size = new System.Drawing.Size(85, 39);
            this.numericUpDownThreshold.TabIndex = 17;
            this.numericUpDownThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownThreshold.Value = new decimal(new int[] {
            127,
            0,
            0,
            0});
            // 
            // labelThreshold
            // 
            this.labelThreshold.AutoSize = true;
            this.labelThreshold.BackColor = System.Drawing.Color.Transparent;
            this.labelThreshold.Font = new System.Drawing.Font("Microsoft JhengHei", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.labelThreshold.Location = new System.Drawing.Point(420, 533);
            this.labelThreshold.Name = "labelThreshold";
            this.labelThreshold.Size = new System.Drawing.Size(60, 25);
            this.labelThreshold.TabIndex = 33;
            this.labelThreshold.Text = "Thd :";
            // 
            // OtsuButton
            // 
            this.OtsuButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.OtsuButton.Location = new System.Drawing.Point(148, 522);
            this.OtsuButton.Name = "OtsuButton";
            this.OtsuButton.Size = new System.Drawing.Size(130, 48);
            this.OtsuButton.TabIndex = 15;
            this.OtsuButton.Text = "Otsu";
            this.OtsuButton.UseVisualStyleBackColor = true;
            this.OtsuButton.Click += new System.EventHandler(this.OtsuButton_Click);
            // 
            // KapurButton
            // 
            this.KapurButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.KapurButton.Location = new System.Drawing.Point(12, 522);
            this.KapurButton.Name = "KapurButton";
            this.KapurButton.Size = new System.Drawing.Size(130, 48);
            this.KapurButton.TabIndex = 14;
            this.KapurButton.Text = "Kapur";
            this.KapurButton.UseVisualStyleBackColor = true;
            this.KapurButton.Click += new System.EventHandler(this.KapurButton_Click);
            // 
            // ConnectedButton
            // 
            this.ConnectedButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ConnectedButton.Location = new System.Drawing.Point(12, 589);
            this.ConnectedButton.Name = "ConnectedButton";
            this.ConnectedButton.Size = new System.Drawing.Size(130, 48);
            this.ConnectedButton.TabIndex = 18;
            this.ConnectedButton.Text = "連通法";
            this.ConnectedButton.UseVisualStyleBackColor = true;
            this.ConnectedButton.Click += new System.EventHandler(this.ConnectedButton_Click);
            // 
            // ConnectedRecursiveButton
            // 
            this.ConnectedRecursiveButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ConnectedRecursiveButton.Location = new System.Drawing.Point(148, 589);
            this.ConnectedRecursiveButton.Name = "ConnectedRecursiveButton";
            this.ConnectedRecursiveButton.Size = new System.Drawing.Size(166, 48);
            this.ConnectedRecursiveButton.TabIndex = 19;
            this.ConnectedRecursiveButton.Text = "連通法(遞迴)";
            this.ConnectedRecursiveButton.UseVisualStyleBackColor = true;
            this.ConnectedRecursiveButton.Click += new System.EventHandler(this.ConnectedRecursiveButton_Click);
            // 
            // WatershedButton
            // 
            this.WatershedButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.WatershedButton.Location = new System.Drawing.Point(148, 122);
            this.WatershedButton.Name = "WatershedButton";
            this.WatershedButton.Size = new System.Drawing.Size(130, 48);
            this.WatershedButton.TabIndex = 3;
            this.WatershedButton.Text = "分水嶺";
            this.WatershedButton.UseVisualStyleBackColor = true;
            this.WatershedButton.Click += new System.EventHandler(this.WatershedButton_Click);
            // 
            // KmeanButton
            // 
            this.KmeanButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.KmeanButton.Location = new System.Drawing.Point(12, 656);
            this.KmeanButton.Name = "KmeanButton";
            this.KmeanButton.Size = new System.Drawing.Size(130, 48);
            this.KmeanButton.TabIndex = 20;
            this.KmeanButton.Text = "K-mean";
            this.KmeanButton.UseVisualStyleBackColor = true;
            this.KmeanButton.Click += new System.EventHandler(this.KmeanButton_Click);
            // 
            // KDtreeButton
            // 
            this.KDtreeButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.KDtreeButton.Location = new System.Drawing.Point(148, 656);
            this.KDtreeButton.Name = "KDtreeButton";
            this.KDtreeButton.Size = new System.Drawing.Size(130, 48);
            this.KDtreeButton.TabIndex = 21;
            this.KDtreeButton.Text = "K-D tree";
            this.KDtreeButton.UseVisualStyleBackColor = true;
            // 
            // FuzzyCmeanButton
            // 
            this.FuzzyCmeanButton.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.FuzzyCmeanButton.Location = new System.Drawing.Point(284, 656);
            this.FuzzyCmeanButton.Name = "FuzzyCmeanButton";
            this.FuzzyCmeanButton.Size = new System.Drawing.Size(130, 48);
            this.FuzzyCmeanButton.TabIndex = 22;
            this.FuzzyCmeanButton.Text = "FCM";
            this.FuzzyCmeanButton.UseVisualStyleBackColor = true;
            this.FuzzyCmeanButton.Click += new System.EventHandler(this.FuzzyCmeanButton_Click);
            // 
            // numericUpDownK
            // 
            this.numericUpDownK.Font = new System.Drawing.Font("Microsoft JhengHei", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.numericUpDownK.Location = new System.Drawing.Point(461, 655);
            this.numericUpDownK.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownK.Name = "numericUpDownK";
            this.numericUpDownK.Size = new System.Drawing.Size(85, 39);
            this.numericUpDownK.TabIndex = 23;
            this.numericUpDownK.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDownK.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelK
            // 
            this.labelK.AutoSize = true;
            this.labelK.BackColor = System.Drawing.Color.Transparent;
            this.labelK.Font = new System.Drawing.Font("Microsoft JhengHei", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.labelK.Location = new System.Drawing.Point(420, 669);
            this.labelK.Name = "labelK";
            this.labelK.Size = new System.Drawing.Size(35, 25);
            this.labelK.TabIndex = 34;
            this.labelK.Text = "K :";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 721);
            this.Controls.Add(this.labelK);
            this.Controls.Add(this.numericUpDownK);
            this.Controls.Add(this.FuzzyCmeanButton);
            this.Controls.Add(this.KDtreeButton);
            this.Controls.Add(this.KmeanButton);
            this.Controls.Add(this.WatershedButton);
            this.Controls.Add(this.ConnectedRecursiveButton);
            this.Controls.Add(this.ConnectedButton);
            this.Controls.Add(this.KapurButton);
            this.Controls.Add(this.OtsuButton);
            this.Controls.Add(this.labelThreshold);
            this.Controls.Add(this.numericUpDownThreshold);
            this.Controls.Add(this.ThresholdingButton);
            this.Controls.Add(this.MarrHildrethButton);
            this.Controls.Add(this.HoughLinesButton);
            this.Controls.Add(this.MedianFilterButton);
            this.Controls.Add(this.numericUpDownV);
            this.Controls.Add(this.labelV);
            this.Controls.Add(this.numericUpDownS);
            this.Controls.Add(this.labelS);
            this.Controls.Add(this.labelH);
            this.Controls.Add(this.ShowOriginalImageButton);
            this.Controls.Add(this.numericUpDownH);
            this.Controls.Add(this.FuzzyMedianFilterButton);
            this.Controls.Add(this.HSVButton);
            this.Controls.Add(this.HistogramEqualizationButton);
            this.Controls.Add(this.MeanFilterButton);
            this.Controls.Add(this.GrayScaleButton);
            this.Controls.Add(this.LoadImageButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Demonstrate";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownK)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LoadImageButton;
        private System.Windows.Forms.Button GrayScaleButton;
        private System.Windows.Forms.Button MeanFilterButton;
        private System.Windows.Forms.Button HistogramEqualizationButton;
        private System.Windows.Forms.Button HSVButton;
        private System.Windows.Forms.Button FuzzyMedianFilterButton;
        private System.Windows.Forms.NumericUpDown numericUpDownH;
        private System.Windows.Forms.Button ShowOriginalImageButton;
        private System.Windows.Forms.Label labelH;
        private System.Windows.Forms.Label labelS;
        private System.Windows.Forms.NumericUpDown numericUpDownS;
        private System.Windows.Forms.Label labelV;
        private System.Windows.Forms.NumericUpDown numericUpDownV;
        private System.Windows.Forms.Button MedianFilterButton;
        private Button HoughLinesButton;
        private Button MarrHildrethButton;
        private Button ThresholdingButton;
        private NumericUpDown numericUpDownThreshold;
        private Label labelThreshold;
        private Button OtsuButton;
        private Button KapurButton;
        private Button ConnectedButton;
        private Button ConnectedRecursiveButton;
        private Button WatershedButton;
        private Button KmeanButton;
        private Button KDtreeButton;
        private Button FuzzyCmeanButton;
        private NumericUpDown numericUpDownK;
        private Label labelK;
    }
}
